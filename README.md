# Tuxblocs

Tuxblocs est un logiciel qui permet de représenter les milliers, centaines, dizaines et unités sous fomes de blocs de base.
 Tuxblocs est un logiciel qui permet de représenter les milliers, centaines, dizaines et unités sous fomes de blocs de base, et d'effectuer des conversions entre colonnes.
 Il aide à comprendre le système de numération positionnelle.
 Ce n'est pas un exerciseur autonome pour l'élève. Il s'agit d'un outil de représentation, au même titre que le matériel de manipulation que l'on trouve dans les classes.
 https://forge.apps.education.fr/educajou/tuxblocs
 Arnaud Champollion